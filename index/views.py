from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.db import IntegrityError
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from .models import User, Choices, Questions, Answer, Form, Responses

# from django.core import serializers
from .words import word
import json
import random
import string
import csv
from django.views.decorators.csrf import csrf_exempt

import requests
from gql import gql, Client
from gql.transport.aiohttp import AIOHTTPTransport
# Create your views here.
from django.conf import settings

def index(request):
    print('request', request)
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('login'))
    forms = Form.objects.filter(creator=request.user)
    return render(request, "index/index.html", {
        "word": word,
        "forms": forms,
        "user": request.user
    })

@csrf_exempt
def login_view_directly(request):
   # Check if the user is logged in
    print('nhi test 123', request.body)
    data = json.loads(request.body)
    print('data', data)
    # data = request.POST

    if request.user.is_authenticated:
        print('session', request.session.session_key)
        return HttpResponse(request.session.session_key)
    if request.method == "POST":
        userid = data['id']
        # username = data['username'].lower()
        username = str(userid) + 'hokk'
        email = str(userid) + data['email']
        
        print('before auth', username, email)
        # if User.objects.filter(username=username).exists():
        #     print('exist user')
        # else:
        #     print('does not exist user')
        user = authenticate(request, username=username, password='10710p7')

        # if user authentication success
        if user is not None:
            login(request, user)
            # return HttpResponseRedirect(reverse('index'))
            print('session', request.session.session_key)
            return HttpResponse(request.session.session_key)
        else:
            # Register new account base on current username by default
            # email = username + 'default@gmail.com'
            print('register')
            # try:
            user = User.objects.create_user(
                username=username, password='10710p7', email=email)
            user.save()
            login(request, user)
            return HttpResponse(request.session.session_key)
            # except IntegrityError:
            #     return render(request, "index/register.html", {
            #         "message": word.get('ruleUsername'),
            #         "word": word,
            #     })
    return HttpResponse('no data')


# def login_view_directly(request, username, email):
#     if request.user.is_authenticated:
#         return HttpResponseRedirect(reverse('index'))
#     print(username)
#     print(request.method)
#     user = authenticate(request, username=username, password='defaultpass')
#     if user is not None:
#         login(request, user)
#         return HttpResponseRedirect(reverse('index'))
#     else:
#         # Register new account base on current username by default
#         # email = username + 'default@gmail.com'
#         try:
#             user = User.objects.create_user(
#                 username=username, password='defaultpass', email=email)
#             user.save()
#             login(request, user)
#             return HttpResponseRedirect(reverse('index'))
#         except IntegrityError:
#             return render(request, "index/register.html", {
#                 "message": word.get('ruleUsername'),
#                 "word": word,
#             })
#     return render(request, "index/register.html", {
#         "word": word,
#     })


# @csrf_exempt
def login_view(request):
   # Check if the user is logged in
    # print(request.method)
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('index'))
    if request.method == "POST":
        username = request.POST["username"].lower()
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        # if user authentication success
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            return render(request, "index/login.html", {
                "message": word.get('ruleLogin'),
                "word": word,
            })
    return render(request, "index/login.html", {
        "word": word,
    })


def register(request):
    # Check if the user is logged in
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('index'))
    if request.method == "POST":
        username = request.POST["username"].lower()
        password = request.POST["password"]
        email = request.POST["email"]
        confirmation = request.POST["confirmation"]
        # check if the password is the same as confirmation
        if password != confirmation:
            return render(request, "index/register.html", {
                "message": word.get('rulePassword'),
                "word": word,
            })
        # Checks if the username is already in use
        if User.objects.filter(email=email).count() == 1:
            return render(request, "index/register.html", {
                "message": word.get('ruleEmail'),
                "word": word,
            })
        try:
            user = User.objects.create_user(
                username=username, password=password, email=email)
            user.save()
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
        except IntegrityError:
            return render(request, "index/register.html", {
                "message": word.get('ruleUsername'),
                "word": word,
            })
    return render(request, "index/register.html", {
        "word": word,
    })


def logout_view(request):
    # Logout the user
    logout(request)
    return HttpResponseRedirect(reverse('index'))


def create_form(request):
    # Creator must be authenticated
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    # Create a blank form API
    if request.method == "POST":
        print(word.get('hi'))
        data = json.loads(request.body)
        title = data["title"]
        code = ''.join(random.choice(string.ascii_letters + string.digits)
                       for x in range(30))
        choices = Choices(choice=word.get('option'))
        choices.save()
        question = Questions(question_type="multiple choice", question=word.get(
            'untitledQuestion'), required=False)
        question.save()
        question.choices.add(choices)
        question.save()
        form = Form(code=code, title=title, creator=request.user)
        form.save()
        form.questions.add(question)
        form.save()
        return JsonResponse({"message": "Sucess", "code": code})


def edit_form(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse("404"))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    return render(request, "index/form.html", {
        "word": word,
        "code": code,
        "form": formInfo
    })


def edit_title(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse("404"))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        if len(data["title"]) > 0:
            formInfo.title = data["title"]
            formInfo.save()
        else:
            formInfo.title = formInfo.title[0]
            formInfo.save()
        return JsonResponse({"message": "Success", "title": formInfo.title})


def edit_description(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse("404"))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        formInfo.description = data["description"]
        formInfo.save()
        return JsonResponse({"message": "Success", "description": formInfo.description})


def edit_bg_color(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse("404"))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        formInfo.background_color = data["bgColor"]
        formInfo.save()
        return JsonResponse({"message": "Success", "bgColor": formInfo.background_color})


def edit_text_color(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse("404"))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        formInfo.text_color = data["textColor"]
        formInfo.save()
        return JsonResponse({"message": "Success", "textColor": formInfo.text_color})


def edit_setting(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse("404"))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        formInfo.collect_email = data["collect_email"]
        # formInfo.is_quiz = data["is_quiz"]
        # formInfo.authenticated_responder = data["authenticated_responder"]
        formInfo.confirmation_message = data["confirmation_message"]
        formInfo.edit_after_submit = data["edit_after_submit"]
        # formInfo.allow_view_score = data["allow_view_score"]
        formInfo.save()
        return JsonResponse({'message': "Success"})


def delete_form(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse("404"))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "DELETE":
        # Delete all questions and choices
        for i in formInfo.questions.all():
            for j in i.choices.all():
                j.delete()
            i.delete()
        for i in Responses.objects.filter(response_to=formInfo):
            for j in i.response.all():
                j.delete()
            i.delete()
        formInfo.delete()
        return JsonResponse({'message': "Success"})


def edit_question(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        question_id = data["id"]
        question = Questions.objects.filter(id=question_id)
        if question.count() == 0:
            return HttpResponseRedirect(reverse("404"))
        else:
            question = question[0]
        question.question = data["question"]
        question.question_type = data["question_type"]
        question.required = data["required"]
        if(data.get("score")):
            question.score = data["score"]
        if(data.get("answer_key")):
            question.answer_key = data["answer_key"]
        question.save()
        return JsonResponse({'message': "Success"})


def edit_choice(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        choice_id = data["id"]
        choice = Choices.objects.filter(id=choice_id)
        if choice.count() == 0:
            return HttpResponseRedirect(reverse("404"))
        else:
            choice = choice[0]
        choice.choice = data["choice"]
        if(data.get('is_answer')):
            choice.is_answer = data["is_answer"]
        choice.save()
        return JsonResponse({'message': "Success"})


def add_choice(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        choice = Choices(choice=word.get('option'))
        choice.save()
        formInfo.questions.get(pk=data["question"]).choices.add(choice)
        formInfo.save()
        return JsonResponse({"message": "Success", "choice": choice.choice, "id": choice.id})


def remove_choice(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        data = json.loads(request.body)
        choice = Choices.objects.filter(pk=data["id"])
        if choice.count() == 0:
            return HttpResponseRedirect(reverse("404"))
        else:
            choice = choice[0]
        choice.delete()
        return JsonResponse({"message": "Success"})


def get_choice(request, code, question):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "GET":
        question = Questions.objects.filter(id=question)
        if question.count() == 0:
            return HttpResponseRedirect(reverse('404'))
        else:
            question = question[0]
        choices = question.choices.all()
        choices = [{"choice": i.choice, "is_answer": i.is_answer, "id": i.id}
                   for i in choices]
        return JsonResponse({"choices": choices, "question": question.question, "question_type": question.question_type, "question_id": question.id})


def add_question(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "POST":
        choices = Choices(choice=word.get('option'))
        choices.save()
        question = Questions(question_type="multiple choice", question=word.get(
            'untitledQuestion'), required=False)
        question.save()
        question.choices.add(choices)
        question.save()
        formInfo.questions.add(question)
        formInfo.save()
        return JsonResponse({'question': {'question': word.get('untitledQuestion'), "question_type": "multiple choice", "required": False, "id": question.id},
                             "choices": {"choice": word.get('option'), "is_answer": False, 'id': choices.id}})


def delete_question(request, code, question):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "DELETE":
        question = Questions.objects.filter(id=question)
        # print('question', question[0])
        if question.count() == 0:
            return HttpResponseRedirect(reverse("404"))
        else:
            question = question[0]
        for i in question.choices.all():
            i.delete()
            print('delete main')
        question.delete()  
        return JsonResponse({"message": "Success"})


def score(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if not formInfo.is_quiz:
        return HttpResponseRedirect(reverse("edit_form", args=[code]))
    else:
        return render(request, "index/score.html", {
            "word": word,
            "form": formInfo
        })


def edit_score(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if not formInfo.is_quiz:
        return HttpResponseRedirect(reverse("edit_form", args=[code]))
    else:
        if request.method == "POST":
            data = json.loads(request.body)
            question_id = data["question_id"]
            question = formInfo.questions.filter(id=question_id)
            if question.count() == 0:
                return HttpResponseRedirect(reverse("edit_form", args=[code]))
            else:
                question = question[0]
            score = data["score"]
            if score == "":
                score = 0
            question.score = score
            question.save()
            return JsonResponse({"message": "Success"})


def answer_key(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if not formInfo.is_quiz:
        return HttpResponseRedirect(reverse("edit_form", args=[code]))
    else:
        if request.method == "POST":
            data = json.loads(request.body)
            question = Questions.objects.filter(id=data["question_id"])
            if question.count() == 0:
                return HttpResponseRedirect(reverse("edit_form", args=[code]))
            else:
                question = question[0]
            if question.question_type == "short" or question.question_type == "paragraph":
                question.answer_key = data["answer_key"]
                question.save()
            else:
                for i in question.choices.all():
                    i.is_answer = False
                    i.save()
                if question.question_type == "multiple choice":
                    choice = question.choices.get(pk=data["answer_key"])
                    choice.is_answer = True
                    choice.save()
                else:
                    for i in data["answer_key"]:
                        choice = question.choices.get(id=i)
                        choice.is_answer = True
                        choice.save()
                question.save()
            return JsonResponse({'message': "Success"})


def feedback(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if not formInfo.is_quiz:
        return HttpResponseRedirect(reverse("edit_form", args=[code]))
    else:
        if request.method == "POST":
            data = json.loads(request.body)
            question = formInfo.questions.get(id=data["question_id"])
            question.feedback = data["feedback"]
            question.save()
            return JsonResponse({'message': "Success"})


def view_form(request, code):
    formInfo = Form.objects.filter(code=code)
    eventid = request.GET.get('eventid', None)
    userid = request.GET.get('userid', None)
    print('test viewform ', eventid, userid)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    if formInfo.authenticated_responder:
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("login"))
    return render(request, "index/view_form.html", {
        "form": formInfo,
        "word": word,
        "eventid": eventid,
        "userid": userid
    })


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def submit_form(request, code, eventid, userid):
    formInfo = Form.objects.filter(code=code)
    print(eventid)
    if eventid != 'None' and userid != 'None':
        # eventidInt = int(eventid)
        # useridInt = int(userid)
        print('thu xem sao /submit', code, eventid, userid)
        transport = AIOHTTPTransport(url=settings.EXTERNAL_API_URL)
        client = Client(transport=transport, fetch_schema_from_transport=True)
        query = gql(
            """
                mutation responseForm($userId: String!, $eventId: String!){
                    responseForm(userId: $userId, eventId: $eventId)
            }
            """
        )
        params = {"userId": userid, "eventId": eventid}
        result = client.execute(query, variable_values=params)
        print(result)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    if formInfo.authenticated_responder:
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("login"))
    if request.method == "POST":
        code = ''.join(random.choice(string.ascii_letters + string.digits)
                       for x in range(20))
        if formInfo.authenticated_responder:
            response = Responses(response_code=code, response_to=formInfo,
                                 responder_ip=get_client_ip(request), responder=request.user)
            response.save()
        else:
            if not formInfo.collect_email:
                response = Responses(
                    response_code=code, response_to=formInfo, responder_ip=get_client_ip(request))
                response.save()
            else:
                response = Responses(response_code=code, response_to=formInfo, responder_ip=get_client_ip(
                    request), responder_email=request.POST["email-address"])
                response.save()

        for i in request.POST:
            # Excluding csrf token
            if i == "csrfmiddlewaretoken" or i == "email-address":
                continue
            question = formInfo.questions.get(id=i)
            for j in request.POST.getlist(i):
                answer = Answer(answer=j, answer_to=question)
                answer.save()
                response.response.add(answer)
                response.save()
        return render(request, "index/form_response.html", {
            "word": word,
            "form": formInfo,
            "code": code
        })


def responses(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    # print(Responses.objects.filter(response_to = formInfo).values())
    return render(request, "index/responses.html", {
        "word": word,
        "form": formInfo,
        "responses": Responses.objects.filter(response_to=formInfo)
    })


def response(request, code, response_code):
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if not formInfo.allow_view_score:
        if formInfo.creator != request.user:
            return HttpResponseRedirect(reverse("403"))
    total_score = 0
    score = 0
    responseInfo = Responses.objects.filter(response_code=response_code)
    if responseInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        responseInfo = responseInfo[0]
    if formInfo.is_quiz:
        for i in formInfo.questions.all():
            total_score += i.score
        for i in responseInfo.response.all():
            if i.answer_to.question_type == "short" or i.answer_to.question_type == "paragraph":
                if i.answer == i.answer_to.answer_key:
                    score += i.answer_to.score
            elif i.answer_to.question_type == "multiple choice":
                answerKey = None
                for j in i.answer_to.choices.all():
                    if j.is_answer:
                        answerKey = j.id
                if answerKey is not None and int(answerKey) == int(i.answer):
                    score += i.answer_to.score
        _temp = []
        for i in responseInfo.response.all():
            if i.answer_to.question_type == "checkbox" and i.answer_to.pk not in _temp:
                answers = []
                answer_keys = []
                for j in responseInfo.response.filter(answer_to__pk=i.answer_to.pk):
                    answers.append(int(j.answer))
                    for k in j.answer_to.choices.all():
                        if k.is_answer and k.pk not in answer_keys:
                            answer_keys.append(k.pk)
                    _temp.append(i.answer_to.pk)
                if answers == answer_keys:
                    score += i.answer_to.score
    print(formInfo.questions.all())
    for question in formInfo.questions.all():
        for choice in question.choices.all():
            print("choice.pk:", choice.pk, ",question.pk:", question.pk,
                  ",question.question:", question.question, ",choice.is_answer:", choice.is_answer)
    for res in responseInfo.response.all():
        print("res:", res.answer_to.pk, res.answer)
    return render(request, "index/response.html", {
        "word": word,
        "form": formInfo,
        "response": responseInfo,
        "score": score,
        "total_score": total_score
    })


def edit_response(request, code, response_code):
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    response = Responses.objects.filter(
        response_code=response_code, response_to=formInfo)
    if response.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        response = response[0]
    if formInfo.authenticated_responder:
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("login"))
        if response.responder != request.user:
            return HttpResponseRedirect(reverse('403'))
    if request.method == "POST":
        if formInfo.authenticated_responder and not response.responder:
            response.responder = request.user
            response.save()
        if formInfo.collect_email:
            response.responder_email = request.POST["email-address"]
            response.save()
        # Deleting all existing answers
        for i in response.response.all():
            i.delete()
        for i in request.POST:
            # Excluding csrf token and email address
            if i == "csrfmiddlewaretoken" or i == "email-address":
                continue
            question = formInfo.questions.get(id=i)
            for j in request.POST.getlist(i):
                answer = Answer(answer=j, answer_to=question)
                answer.save()
                response.response.add(answer)
                response.save()
        if formInfo.is_quiz:
            return HttpResponseRedirect(reverse("response", args=[formInfo.code, response.response_code]))
        else:
            return render(request, "index/form_response.html", {
                "form": formInfo,
                "code": response.response_code,
                "word": word,
            })
    return render(request, "index/edit_response.html", {
        "form": formInfo,
        "word": word,
        "response": response
    })


def contact_form_template(request):
    # Creator must be authenticated
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    # Create a blank form API
    if request.method == "POST":
        code = ''.join(random.choice(string.ascii_letters + string.digits)
                       for x in range(30))
        name = Questions(question_type="short", question="名前", required=True)
        name.save()
        email = Questions(question_type="short",
                          question="メールアドレス", required=True)
        email.save()
        address = Questions(question_type="paragraph",
                            question="住所", required=True)
        address.save()
        phone = Questions(question_type="short",
                          question="電話番号", required=False)
        phone.save()
        comments = Questions(question_type="paragraph",
                             question="コメント", required=False)
        comments.save()
        form = Form(code=code, title="連絡先情報", creator=request.user,
                    background_color="#e2eee0", allow_view_score=False, edit_after_submit=True)
        form.save()
        form.questions.add(name)
        form.questions.add(email)
        form.questions.add(address)
        form.questions.add(phone)
        form.questions.add(comments)
        form.save()
        return JsonResponse({"message": "Sucess", "code": code})


def customer_feedback_template(request):
    # Creator must be authenticated
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    # Create a blank form API
    if request.method == "POST":
        code = ''.join(random.choice(string.ascii_letters + string.digits)
                       for x in range(30))
        comment = Choices(choice="コメント")
        comment.save()
        question = Choices(choice="質問")
        question.save()
        bug = Choices(choice="バグレポート")
        bug.save()
        feature = Choices(choice="機能リクエスト")
        feature.save()
        feedback_type = Questions(
            question="フィードバックタイプ", question_type="multiple choice", required=False)
        feedback_type.save()
        feedback_type.choices.add(comment)
        feedback_type.choices.add(bug)
        feedback_type.choices.add(question)
        feedback_type.choices.add(feature)
        feedback_type.save()
        feedback = Questions(question="フィードバックタイプ",
                             question_type="paragraph", required=True)
        feedback.save()
        suggestion = Questions(question="改善のための提案",
                               question_type="paragraph", required=False)
        suggestion.save()
        name = Questions(
            question="名前", question_type="short", required=False)
        name.save()
        email = Questions(question="Eメール",
                          question_type="short", required=False)
        email.save()
        form = Form(code=code, title="お客様の声", creator=request.user, background_color="#A7C8EC", confirmation_message="フィードバックを送信いただき、ありがとうございました",
                    description="お客様満足の向上に向けて、お客様からいただくご意見、ご相談などから検討し、改善を図っていきます。", allow_view_score=False, edit_after_submit=True)
        form.save()
        form.questions.add(feedback_type)
        form.questions.add(feedback)
        form.questions.add(suggestion)
        form.questions.add(name)
        form.questions.add(email)
        return JsonResponse({"message": "Sucess", "code": code})


def event_registration_template(request):
    # Creator must be authenticated
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    # Create a blank form API
    if request.method == "POST":
        code = ''.join(random.choice(string.ascii_letters + string.digits)
                       for x in range(30))
        name = Questions(question="お名前", question_type="short", required=False)
        name.save()
        email = Questions(question="メールアドレス",
                          question_type="short", required=True)
        email.save()
        organization = Questions(
            question="組織", question_type="short", required=True)
        organization.save()
        day1 = Choices(choice="１日目")
        day1.save()
        day2 = Choices(choice="2日目")
        day2.save()
        day3 = Choices(choice="3日目")
        day3.save()
        day = Questions(question="参加日をお選びください。",
                        question_type="checkbox", required=True)
        day.save()
        day.choices.add(day1)
        day.choices.add(day2)
        day.choices.add(day3)
        day.save()
        dietary_none = Choices(choice="なし")
        dietary_none.save()
        dietary_vegetarian = Choices(choice="ベジタリアン")
        dietary_vegetarian.save()
        dietary_kosher = Choices(choice="コーシャ")
        dietary_kosher.save()
        dietary_gluten = Choices(choice="グルテンフリー")
        dietary_gluten.save()
        dietary = Questions(
            question="食事制限", question_type="multiple choice", required=True)
        dietary.save()
        dietary.choices.add(dietary_none)
        dietary.choices.add(dietary_vegetarian)
        dietary.choices.add(dietary_gluten)
        dietary.choices.add(dietary_kosher)
        dietary.save()
        accept_agreement = Choices(choice="はい")
        accept_agreement.save()
        agreement = Questions(
            question="到着時に xx 円を支払う必要があることを理解しています", question_type="checkbox", required=True)
        agreement.save()
        agreement.choices.add(accept_agreement)
        agreement.save()
        form = Form(code=code, title="イベント参加申込書", creator=request.user, background_color="#fdefc3",
                    confirmation_message="登録受付完了\n\
その他の情報をここに挿入する\n\
\n\
以下のリンクを保存してください。このリンクを使用して、登録の情報を編集できる",
                    description="開催日: 2021年3月17日\n\
開催地: Tokyo, Fuchu, Asahicho, 3 Chome−１１−1\n\
お問い合わせ:(123) 456-7890 または no_reply@example.com", edit_after_submit=True, allow_view_score=False)
        form.save()
        form.questions.add(name)
        form.questions.add(email)
        form.questions.add(organization)
        form.questions.add(day)
        form.questions.add(dietary)
        form.questions.add(agreement)
        form.save()
        return JsonResponse({"message": "Sucess", "code": code})


def delete_responses(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    if request.method == "DELETE":
        responses = Responses.objects.filter(response_to=formInfo)
        for response in responses:
            for i in response.response.all():
                i.delete()
            response.delete()
        return JsonResponse({"message": "Success"})

# Simple CSV Write Operation


def download(request, code):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    formInfo = Form.objects.filter(code=code)
    # Checking if form exists
    if formInfo.count() == 0:
        return HttpResponseRedirect(reverse('404'))
    else:
        formInfo = formInfo[0]
    # Checking if form creator is user
    if formInfo.creator != request.user:
        return HttpResponseRedirect(reverse("403"))
    # Create the HttpResponse object with the appropriate CSV header.
    responses = HttpResponse(content_type='text/csv')
    responses['Content-Disposition'] = 'attachment; filename="Responses.csv"'
    responses.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(responses, dialect='excel')
    writer.writerow([formInfo.title])
    responseInfoArray = Responses.objects.filter(response_to=formInfo)
    # print(responseInfoArray)
    # print(responseInfoArray)
    lst_question = []
    for question in formInfo.questions.all():
        lst_question += [str(question.question)]
        # print(lst_question)
        # for choice in question.choices.all():
        #     print("choice.pk:",choice.pk,"choice.choice:",choice.choice, ",question.pk:",question.pk, ",question.question:",question.question, ",choice.is_answer:",choice.is_answer)
        #     print("=======")
    writer.writerow(lst_question)
    for item in responseInfoArray:
        lst_answer = []
        lst_checkbox = []
        for res in item.response.all():
            if res.answer_to.question_type == "short" or res.answer_to.question_type == "paragraph":
                lst_answer += [str(res.answer)]
            elif res.answer_to.question_type == "multiple choice":
                for question in formInfo.questions.filter(question_type="multiple choice"):
                    for choice in question.choices.all():
                        # print(res.answer, question.pk, choice.choice)
                        if int(res.answer) == int(choice.pk):
                            lst_answer += [str(choice.choice)]
            elif res.answer_to.question_type == "checkbox":
                for question in formInfo.questions.filter(question_type="checkbox"):
                    for choice in question.choices.all():
                        if res.answer_to.pk == question.pk:
                            if int(res.answer) == int(choice.pk):
                                lst_checkbox.append(str(choice.choice))
        # print(lst_checkbox)
        lst_answer += [lst_checkbox]
        # print(lst_answer)
        writer.writerow(lst_answer)
    return responses

# Error handler


def FourZeroThree(request):
    return render(request, "error/403.html")


def FourZeroFour(request):
    return render(request, "error/404.html")
