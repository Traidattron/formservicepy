from django.contrib import admin

# Register your models here.
from .models import User, Form, Choices, Questions, Answer, Responses

# admin.site.register(User)
admin.site.register(Form)
admin.site.register(Choices)
admin.site.register(Questions)
admin.site.register(Answer)
admin.site.register(Responses)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'username', 'is_superuser')
