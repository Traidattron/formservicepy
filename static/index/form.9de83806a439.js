document.addEventListener("DOMContentLoaded", () => {
    let editTitle = document.querySelectorAll(".edit-on-click");
    // console.log(editTitle)
    editTitle.forEach((each) => {
        each.addEventListener("click", () => {
            each.select()
        })
    })
    // editTitle.addEventListener("click", () => {
    //     console.log("nhi jvs")
    //     editTitle.select()
    // })
    const csrf = Cookies.get('csrftoken');
    document.body.style.backgroundColor = document.querySelector("#bg-color").innerHTML;
    document.body.style.color = document.querySelector("#text-color").innerHTML;
    document.querySelectorAll(".txt-clr").forEach(element => {
        element.style.color = document.querySelector("#text-color").innerHTML;
    })
    document.querySelectorAll(".input-form-title").forEach(title => {
        // title.addEventListener("click", ()=>{
        //     title.select()
        // })
        title.addEventListener("input", function () {
            fetch(`edit_title`, {
                method: "POST",
                headers: { 'X-CSRFToken': csrf },
                body: JSON.stringify({
                    "title": this.value
                })

            }).then(res => {
                document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
            })
            document.title = `${this.value} - TGL Forms`
            document.querySelectorAll(".input-form-title").forEach(ele => {
                ele.value = this.value;
            })
        })
    })
    document.querySelector("#input-form-description").addEventListener("input", function () {
        fetch('edit_description', {
            method: "POST",
            headers: { 'X-CSRFToken': csrf },
            body: JSON.stringify({
                "description": this.value
            })
        })
            .then((_res) => {
                document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
            })
    })
    document.querySelectorAll(".textarea-adjust").forEach(tx => {
        tx.style.height = "auto";
        tx.style.height = (10 + tx.scrollHeight) + "px";
        tx.addEventListener('input', e => {
            tx.style.height = "auto";
            tx.style.height = (10 + tx.scrollHeight) + "px";
        })
    })
    document.querySelector("#customize-theme-btn").addEventListener('click', () => {
        document.querySelector("#customize-theme").style.display = "block";
        document.querySelector("#close-customize-theme").addEventListener('click', () => {
            document.querySelector("#customize-theme").style.display = "none";
        })
        window.onclick = e => {
            if (e.target == document.querySelector("#customize-theme")) document.querySelector("#customize-theme").style.display = "none";
        }
    })
    document.querySelector("#input-bg-color").addEventListener("input", function () {
        document.body.style.backgroundColor = this.value;
        fetch('edit_background_color', {
            method: "POST",
            headers: { 'X-CSRFToken': csrf },
            body: JSON.stringify({
                "bgColor": this.value
            })
        })
            .then((_res) => {
                document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
            })
    })
    document.querySelector("#input-text-color").addEventListener("input", function () {
        document.querySelectorAll(".txt-clr").forEach(element => {
            element.style.color = this.value;
        })
        fetch('edit_text_color', {
            method: "POST",
            headers: { 'X-CSRFToken': csrf },
            body: JSON.stringify({
                "textColor": this.value
            })
        }).then((_res) => {
            document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
            setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
        })
    })

    document.querySelectorAll("#send-form-btn").forEach(btn => {
        btn.addEventListener("click", () => {
            document.querySelector("#send-form").style.display = "block";
        })
        document.querySelector("#close-send-form").addEventListener("click", () => {
            document.querySelector("#send-form").style.display = "none";
        })
        window.onclick = e => {
            if (e.target == document.querySelector("#send-form")) document.querySelector("#send-form").style.display = "none";
        }
    })
    document.querySelectorAll("#menu-btn").forEach(btn => {
        btn.addEventListener("click", () => {
            document.querySelector("#menu-smallscreen").style.display = "block";
        })
        document.querySelector("#close-menu-smallscreen").addEventListener("click", () => {
            document.querySelector("#menu-smallscreen").style.display = "none";
        })
        window.onclick = e => {
            if (e.target == document.querySelector("#send-form")) document.querySelector("#send-form").style.display = "none";
        }
    })
    document.querySelector("#copy-btn").addEventListener("click", () => {
        var url = document.getElementById("copy-url");
        navigator.clipboard.writeText(url.value);
        document.querySelector("#send-form").style.display = "none";
    })
    document.querySelector("#setting-form").addEventListener("submit", e => {
        e.preventDefault();
        fetch('edit_setting', {
            method: "POST",
            headers: { 'X-CSRFToken': csrf },
            body: JSON.stringify({
                "collect_email": document.querySelector("#collect_email").checked,
                "is_quiz": document.querySelector("#is_quiz").checked,
                "authenticated_responder": document.querySelector("#authenticated_responder").checked,
                "confirmation_message": document.querySelector("#comfirmation_message").value,
                "edit_after_submit": document.querySelector("#edit_after_submit").checked,
                "allow_view_score": document.querySelector("#allow_view_score").checked,
            })
        }).then((_res) => {
            document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
            setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
        })
        document.querySelector("#setting").style.display = "none";
        if (!document.querySelector("#collect_email").checked) {
            if (document.querySelector(".collect-email")) document.querySelector(".collect-email").parentNode.removeChild(document.querySelector(".collect-email"))
        } else {
            if (!document.querySelector(".collect-email")) {
                let collect_email = document.createElement("div");
                collect_email.classList.add("collect-email")
                collect_email.innerHTML = `<h3 class="question-title">メールアドレス<span class="require-star">*</span></h3>
                <input type="text" autoComplete="off" aria-label="Valid email address" disabled dir = "auto" class="require-email-edit"
                placeholder = "有効なイーメールアドレス" />
                <p class="collect-email-desc">このフォームはメールアドレスを収集しています<span class="open-setting">設定を変更する</span></p>`
                document.querySelector("#form-head").appendChild(collect_email);
                openSetting();
            }
        }
        if (document.querySelector("#is_quiz").checked) {
            if (!document.querySelector("#add-score")) {
                let is_quiz = document.createElement('a')
                is_quiz.setAttribute("href", "score");
                is_quiz.setAttribute("id", "add-score");
                is_quiz.innerHTML = `<img src = "/static/Icon/score.svg" id="add-score" class = "form-option-icon" title = "Add score" alt = "Score icon" />`;
                document.querySelector(".question-options").appendChild(is_quiz)
            }
            if (!document.querySelector(".score")) {
                let quiz_nav = document.createElement("span");
                quiz_nav.classList.add("col-4");
                quiz_nav.classList.add("navigation");
                quiz_nav.classList.add('score');
                quiz_nav.innerHTML = `<a href = "score" class="link">点数</a>`;
                [...document.querySelector(".form-navigation").children].forEach(element => {
                    element.classList.remove("col-6")
                    element.classList.add('col-4')
                })
                document.querySelector(".form-navigation").insertBefore(quiz_nav, document.querySelector(".form-navigation").childNodes[2])
            }
        } else {
            if (document.querySelector("#add-score")) document.querySelector("#add-score").parentNode.removeChild(document.querySelector("#add-score"))
            if (document.querySelector(".score")) {
                [...document.querySelector(".form-navigation").children].forEach(element => {
                    element.classList.remove("col-4")
                    element.classList.add('col-6')
                })
                document.querySelector(".score").parentNode.removeChild(document.querySelector(".score"))
            }
        }
    })
    const openSetting = () => {
        document.querySelectorAll(".open-setting").forEach(ele => {
            ele.addEventListener('click', () => {
                document.querySelector("#setting").style.display = "block";
            })
            document.querySelector("#close-setting").addEventListener('click', () => {
                document.querySelector("#setting").style.display = "none";
            })
            window.onclick = e => {
                if (e.target == document.querySelector("#setting")) document.querySelector("#setting").style.display = "none";
            }
        })
    }
    openSetting();
    // document.querySelector("#delete-form").addEventListener("submit", e => {
    //     e.preventDefault();
    //     if(window.confirm("Are you sure? This action CANNOT be undone.")){
    //         fetch('delete', {
    //             method: "DELETE",
    //             headers: {'X-CSRFToken': csrf}
    //         })
    //         .then(() => window.location = "/")
    //     }
    // })
    document.querySelectorAll("[delete-form]").forEach(btn => {
        btn.addEventListener("submit", (e) => {
            e.preventDefault();
            if (window.confirm("削除します。よろしいですか?")) {
                fetch('delete', {
                    method: "DELETE",
                    headers: { 'X-CSRFToken': csrf }
                })
                    .then(() => window.location = "/")
            }
        })
    })
    // document.querySelector("#delete-form").addEventListener("click", e => {
    //     e.preventDefault();
    //     if(window.confirm("削除します。よろしいですか?")){
    //         fetch('delete', {
    //             method: "DELETE",
    //             headers: {'X-CSRFToken': csrf}
    //         })
    //         .then(() => window.location = "/")
    //     }
    // })
    const editQuestion = () => {
        document.querySelectorAll(".input-question").forEach(question => {
            question.addEventListener("click", function () {
                question.select();
            })
            question.addEventListener('input', function () {
                let question_type;
                let required;
                document.querySelectorAll(".input-question-type").forEach(qp => {
                    if (qp.dataset.id === this.dataset.id) question_type = qp.value
                })
                document.querySelectorAll('.required-checkbox').forEach(rc => {
                    if (rc.dataset.id === this.dataset.id) required = rc.checked;
                })
                fetch('edit_question', {
                    method: "POST",
                    headers: { 'X-CSRFToken': csrf },
                    body: JSON.stringify({
                        id: this.dataset.id,
                        question: this.value,
                        question_type: question_type,
                        required: required
                    })
                })
                    .then((_res) => {
                        document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                        setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
                    })
            })
        })
    }
    editQuestion();

    const editRequire = () => {
        document.querySelectorAll(".required-checkbox").forEach(checkbox => {
            checkbox.addEventListener('input', function () {
                let question;
                let question_type;
                document.querySelectorAll(".input-question-type").forEach(qp => {
                    if (qp.dataset.id === this.dataset.id) question_type = qp.value
                })
                document.querySelectorAll('.input-question').forEach(q => {
                    if (q.dataset.id === this.dataset.id) question = q.value
                })
                fetch('edit_question', {
                    method: "POST",
                    headers: { 'X-CSRFToken': csrf },
                    body: JSON.stringify({
                        id: this.dataset.id,
                        question: question,
                        question_type: question_type,
                        required: this.checked
                    })
                })
                    .then((_res) => {
                        document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                        setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
                    })
            })
        })
    }
    editRequire()
    const editChoice = () => {
        document.querySelectorAll(".edit-choice").forEach(choice => {
            choice.addEventListener("input", function () {
                fetch('edit_choice', {
                    method: "POST",
                    headers: { 'X-CSRFToken': csrf },
                    body: JSON.stringify({
                        "id": this.dataset.id,
                        "choice": this.value
                    })
                }).then((_res) => {
                    document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                    setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
                })
            })
        })
    }
    editChoice()
    const removeOption = () => {
        document.querySelectorAll(".remove-option").forEach(ele => {
            ele.addEventListener("click", function () {
                fetch('remove_choice', {
                    method: "POST",
                    headers: { 'X-CSRFToken': csrf },
                    body: JSON.stringify({
                        "id": this.dataset.id
                    })
                })
                    .then(() => {
                        this.parentNode.parentNode.removeChild(this.parentNode);
                        document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                        setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
                    })
            })
        })
    }
    removeOption()

    const deleteQuestion = () => {
        document.querySelectorAll(".delete-question").forEach(question => {
            question.addEventListener("click", function () {
                fetch(`delete_question/${this.dataset.id}`, {
                    method: "DELETE",
                    headers: { 'X-CSRFToken': csrf },
                })
                    .then(() => {
                        document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                        setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
                        document.querySelectorAll(".question").forEach(q => {
                            if (q.dataset.id === this.dataset.id) {
                                q.parentNode.removeChild(q)
                            }
                        })
                    })
            })
        })
    }
    deleteQuestion()
    const changeType = () => {
        document.querySelectorAll(".input-question-type").forEach(ele => {
            ele.addEventListener('input', function () {
                let required;
                let question;
                document.querySelectorAll('.required-checkbox').forEach(rc => {
                    if (rc.dataset.id === this.dataset.id) required = rc.checked;
                })
                document.querySelectorAll('.input-question').forEach(q => {
                    if (q.dataset.id === this.dataset.id) question = q.value
                })
                fetch('edit_question', {
                    method: "POST",
                    headers: { 'X-CSRFToken': csrf },
                    body: JSON.stringify({
                        id: this.dataset.id,
                        question: question,
                        question_type: this.value,
                        required: required
                    })
                })
                    .then((res) => {
                        document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                        setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
                    })

                if (this.dataset.origin_type === "multiple choice" || this.dataset.origin_type === "checkbox") {
                    document.querySelectorAll(".choices").forEach(choicesElement => {
                        if (choicesElement.dataset.id === this.dataset.id) {
                            if (this.value === "multiple choice" || this.value === "checkbox") {
                                fetch(`get_choice/${this.dataset.id}`, {
                                    method: "GET"
                                })
                                    .then(response => response.json())
                                    .then(result => {
                                        let ele = document.createElement("div");
                                        ele.classList.add('choices');
                                        ele.setAttribute("data-id", result["question_id"])
                                        let choices = '';
                                        if (this.value === "multiple choice") {
                                            for (let i in result["choices"]) {
                                                if (i) {
                                                    choices += `<div class="choice">
                                            <input type="radio" id="${result["choices"][i].id}" disabled>
                                            <label for="${result["choices"][i].id}">
                                                <input type="text" data-id="${result["choices"][i].id}" class="edit-choice" value="${result["choices"][i].choice}">
                                            </label>
                                            <span class="remove-option" title="Remove" data-id="${result["choices"][i].id}">&times;</span></div>`
                                                }
                                            }
                                        } else if (this.value === "checkbox") {
                                            for (let i in result["choices"]) {
                                                if (i) {
                                                    choices += `<div class="choice">
                                            <input type="checkbox" id="${result["choices"][i].id}" disabled>
                                            <label for="${result["choices"][i].id}">
                                                <input type="text" data-id="${result["choices"][i].id}" class="edit-choice" value="${result["choices"][i].choice}">
                                            </label>
                                            <span class="remove-option" title="Remove" data-id="${result["choices"][i].id}">&times;</span></div>`
                                                }
                                            }
                                        }
                                        ele.innerHTML = `<div class="choice">${choices}</div>
                                    <div class="choice-add">
                                        <button for = "add-choice" class="add-option" id="add-option" data-question="${result["question_id"]}"
                                        data-type = "${this.value}">質問を追加</button>
                                    </div>`;
                                        choicesElement.parentNode.replaceChild(ele, choicesElement);
                                        editChoice()
                                        removeOption()
                                        changeType()
                                        editQuestion()
                                        editRequire()
                                        addOption()
                                        deleteQuestion()
                                    })
                            } else {
                                if (this.value === "short") {
                                    choicesElement.parentNode.removeChild(choicesElement)
                                    let ele = document.createElement("div");
                                    ele.innerHTML = `<div class="answers" data-id="${this.dataset.id}">
                                    <input type ="text" class="short-answer" disabled placeholder="記述式テキスト（短文回答）" />
                                </div>`
                                    this.parentNode.insertBefore(ele, this.parentNode.childNodes[4])
                                } else if (this.value === "paragraph") {
                                    choicesElement.parentNode.removeChild(choicesElement)
                                    let ele = document.createElement("div");
                                    ele.innerHTML = `<div class="answers" data-id="${this.dataset.id}">
                                    <textarea class="long-answer" disabled placeholder="記述式テキスト（長文回答）" ></textarea>
                                </div>`
                                    this.parentNode.insertBefore(ele, this.parentNode.childNodes[4])
                                }
                            }
                        }
                    })
                } else {
                    document.querySelectorAll(".question-box").forEach(question => {
                        document.querySelectorAll(".answers").forEach(answer => {
                            if (answer.dataset.id === this.dataset.id) {
                                answer.parentNode.removeChild(answer)
                            }
                        })
                        if ((this.value === "multiple choice" || this.value === "checkbox") && question.dataset.id === this.dataset.id) {
                            fetch(`get_choice/${this.dataset.id}`, {
                                method: "GET"
                            })
                                .then(response => response.json())
                                .then(result => {
                                    let ele = document.createElement("div");
                                    ele.classList.add('choices');
                                    ele.setAttribute("data-id", result["question_id"])
                                    let choices = '';
                                    if (this.value === "multiple choice") {
                                        for (let i in result["choices"]) {
                                            if (i) {
                                                choices += `<div class="choice">
                                        <input type="radio" id="${result["choices"][i].id}" disabled>
                                        <label for="${result["choices"][i].id}">
                                            <input type="text" data-id="${result["choices"][i].id}" class="edit-choice" value="${result["choices"][i].choice}">
                                        </label>
                                        <span class="remove-option" title="Remove" data-id="${result["choices"][i].id}">&times;</span></div>`
                                            }
                                        }
                                    } else if (this.value === "checkbox") {
                                        for (let i in result["choices"]) {
                                            if (i) {
                                                choices += `<div class="choice">
                                        <input type="checkbox" id="${result["choices"][i].id}" disabled>
                                        <label for="${result["choices"][i].id}">
                                            <input type="text" data-id="${result["choices"][i].id}" class="edit-choice" value="${result["choices"][i].choice}">
                                        </label>
                                        <span class="remove-option" title="Remove" data-id="${result["choices"][i].id}">&times;</span></div>`
                                            }
                                        }
                                    }
                                    ele.innerHTML = `<div class="choice">${choices}</div>
                                <div class="choice-add">
                                    <button for = "add-choice" class="add-option" id="add-option" data-question="${result["question_id"]}"
                                    data-type = "${this.value}">質問を追加</button>
                                </div>`;
                                    question.insertBefore(ele, question.childNodes[4])
                                    editChoice()
                                    removeOption()
                                    changeType()
                                    editQuestion()
                                    editRequire()
                                    addOption()
                                    deleteQuestion()
                                })
                        } else {
                            if (this.value === "short") {
                                let ele = document.createElement("div");
                                ele.innerHTML = `<div class="answers" data-id="${this.dataset.id}">
                                <input type ="text" class="short-answer" disabled placeholder="記述式テキスト（短文回答）" />
                            </div>`
                                this.parentNode.insertBefore(ele, this.parentNode.childNodes[4])
                            } else if (this.value === "paragraph") {
                                let ele = document.createElement("div");
                                ele.innerHTML = `<div class="answers" data-id="${this.dataset.id}">
                                <input class="long-answer" disabled placeholder="記述式テキスト（長文回答）" ></input>
                            </div>`
                                this.parentNode.insertBefore(ele, this.parentNode.childNodes[4])
                            }
                        }
                    })
                }
                this.setAttribute("data-origin_type", this.value);
            })
        })
    }
    changeType()
    // const debounce = (func, delay) => {
    //     let inDebounce
    //     return function() {
    //         const context = this
    //         const args = arguments
    //         clearTimeout(inDebounce)
    //         inDebounce = setTimeout(() => func.apply(context, args), delay)
    //     }
    // }
    // function throttle (callback, limit) {
    //     var waiting = false;                      // Initially, we're not waiting
    //     return function () {                      // We return a throttled function
    //         if (!waiting) {                       // If we're not waiting
    //             callback.apply(this, arguments);  // Execute users function
    //             waiting = true;                   // Prevent future invocations
    //             setTimeout(function () {          // After a period of time
    //                 waiting = false;              // And allow future invocations
    //             }, limit);
    //         }
    //     }
    // }
    function addOPT(dataset) {
        console.log(dataset,'aaa')
            fetch('add_choice', {
                method: "POST",
                headers: { 'X-CSRFToken': csrf },
                body: JSON.stringify({
                    "question": dataset.question
                })
            })
            .then(response => response.json())
            .then(result => {
                document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
                let element = document.createElement("div");
                element.classList.add('choice');
                if (dataset.type === "multiple choice") {
                    element.innerHTML = `<input type="radio" id="${result["id"]}" disabled>
                <label for="${result["id"]}">
                    <input type="text" value="${result["choice"]}" class="edit-choice" data-id="${result["id"]}">
                </label>
                <span class="remove-option" title = "Remove" data-id="${result["id"]}">&times;</span>`;
                } else if (dataset.type === "checkbox") {
                    element.innerHTML = `<input type="checkbox" id="${result["id"]}" disabled>
                <label for="${result["id"]}">
                    <input type="text" value="${result["choice"]}" class="edit-choice" data-id="${result["id"]}">
                </label>
                <span class="remove-option" title = "Remove" data-id="${result["id"]}">&times;</span>`;
                }
                document.querySelectorAll(".choices").forEach(choices => {
                    if (choices.dataset.id === dataset.question) {
                        choices.insertBefore(element, choices.childNodes[choices.childNodes.length - 2]);
                        editChoice()
                        removeOption()
                    }
                });
            })
       
    }
    function addOption() {
        document.querySelectorAll(".add-option").forEach(button => {
            button.addEventListener("click", (function () {
                addOPT(this.dataset)
            }))
        })
    }
    addOption();
    document.querySelector("#add-question").addEventListener("click", () => {
        fetch('add_question', {
            method: "POST",
            headers: { 'X-CSRFToken': csrf },
            body: JSON.stringify({})
        })
            .then(response => response.json())
            .then(result => {
                console.log(result)
                document.getElementById("auto-save").innerHTML = "変更内容が保存されました";
                setTimeout(function () { document.getElementById("auto-save").innerHTML = "" }, 5000);
                let ele = document.createElement('div')
                ele.classList.add('margin-top-bottom');
                ele.classList.add('box-shadow');
                ele.classList.add('box');
                ele.classList.add('fade-in');
                ele.classList.add('question-box');
                ele.classList.add('question');
                ele.setAttribute("data-id", result["question"].id)
                ele.innerHTML = `
            <input type="text" data-id="${result["question"].id}" class="question-title edit-on-click input-question" value="${result["question"].question}">
            <select class="question-type-select input-question-type" data-id="${result["question"].id}" data-origin_type = "${result["question"].question_type}">
                <option value="short">記述式</option>
                <option value="paragraph">段落</option>
                <option value="multiple choice" selected>ラジオボタン</option>
                <option value="checkbox">チェックボックス</option>
            </select>
            <div class="choices" data-id="${result["question"].id}">
                <div class="choice">
                    <input type="radio" id="${result["choices"].id}" disabled>
                    <label for="${result["choices"].id}">
                        <input type="text" value="${result["choices"].choice}" class="edit-choice" data-id="${result["choices"].id}">
                    </label>
                    <span class="remove-option" title = "Remove" data-id="${result["choices"].id}">&times;</span>
                </div>
                <div class="choice-add">
                    <button for="add-choice" class="add-option" id="add-option" data-question="${result["question"].id}"
                    data-type = "${result["question"].question_type}">質問を追加</button>
                </div>
            </div>
            <div class="choice-option">
                <div class="choice-option-required">
                    <input type="checkbox" class="required-checkbox" id="${result["question"].id}" data-id="${result["question"].id}">
                    <label for="${result["question"].id}" class="required">必須</label>
                </div>
                <div class="float-right">
                    <img src="/static/Icon/del.svg" alt="Delete question icon" class="question-option-icon delete-question" title="Delete question"
                    data-id="${result["question"].id}">
                </div>
            </div>
            `;
                document.querySelector(".container-form").appendChild(ele);
                document.querySelectorAll(`.question [data-id="${result["question"].id}"]`).forEach(question => {
                    question.querySelectorAll(".add-option").forEach(add => {
                        add.addEventListener("click",() => addOPT(add.dataset));
                    })
                })
                editChoice()
                removeOption()
                changeType()
                editQuestion()
                editRequire()
                deleteQuestion()
                ele.scrollTop = ele.scrollHeight;
            })
        })
    })